package com.java.Editor;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

public class MCanvas extends JPanel{
   
    private Entity entity;

    public MCanvas(Entity entity) {
        this.entity = entity;
    }

    public MCanvas() { }


    public void paintComponent(Graphics g)
    {
        this.entity.setGraphics(g);
        this.entity.draw(g);
    }
}
