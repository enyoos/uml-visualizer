package com.java.Editor;

import java.awt.Color;
import java.awt.Graphics;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.lang.reflect.Parameter;

public class Entity {

    // private static final Color BLACK = Color.BLACK;
    // private static final Color WHITE = Color.WHITE;
    private static final int WRatio = 10;
    private static final int HRatio = 13;
    private static int offSet = 0;
    private static final Color BLACK = new Color(0, 0, 0);
    private static final Color WHITE = new Color(255, 255, 255);
    private String[] methods = null ;    
    private Graphics g;    
    private String[] fields  = null ;    
    private Class<?>[] superClasses;
    private Class<?>[] interfaces;
    private Class<?> cls;


    public Entity ( Class<?> cls )
    {
        this.methods = Utils.buildMethods(cls);
        this.fields  = Utils.buildFields(cls);
        this.cls = cls;
        buildExtensionDependencyScheme(cls);
        // to continue here 
    }

    public void setGraphics ( Graphics ctx ) { this.g = ctx; }

    private int[] computeDimension(){
        int[] ret = null;
        
        // sort the fields and method 
        String[] methodsClone = this.methods.clone(); 
        String[] fieldsClone  = this.fields.clone();
        System.out.println(Arrays.toString ( methodsClone ) );
        System.out.println(Arrays.toString(fieldsClone) );

        int maxW = 0;
        int maxH = 0;

        // TODO : refactor this codee..

        int bIdx = 0;
        int eIDx = methodsClone.length - 1;
        Utils.quickSort( methodsClone, bIdx, eIDx);
        eIDx = fieldsClone.length - 1;
        Utils.quickSort( fieldsClone , bIdx, eIDx );

        // check if the arrays are not empty too...
        int fL = fieldsClone.length == 0 ?  0 :  fieldsClone.length  - 1  ;
        int mL = methodsClone.length == 0 ? 0 :  methodsClone.length - 1;

        String lStringField = fieldsClone[fL];
        String lStringMethod = methodsClone[mL];

        System.out.println("the longest field : " + lStringField);
        System.out.println("the longest method: " + lStringMethod);
        
        // this is extremely bad to watch ... 
        maxW = lStringField.length() > lStringMethod.length() ? Utils.getWStr(lStringField, g) : Utils.getWStr( lStringMethod, g) ;
        int totalEntries = fieldsClone.length + methodsClone.length;

        maxH = totalEntries *  HRatio;
        offSet = maxH / ( int ) totalEntries;

        ret = new int[]{maxW, maxH};

        System.out.println("the max W : " + maxW);

        return ret;
    }


    // only checks for superClasses
    private void buildExtensionDependencyScheme ( Class<?> cls )
    {
        ArrayList<Class<?>> temp = new ArrayList<>();
        buildExtensionDependencySchemeExtension(cls, temp);
        // this.superClasses = temp.toArray(superClasses);
    }

    private void buildExtensionDependencySchemeExtension (  Class<?> cls , ArrayList<Class<?>> arrList )
    {
        String nameClass = cls.getSimpleName();
        if ( Utils.isIn( nameClass ) ) return;
        arrList.add( cls );
        buildExtensionDependencySchemeExtension(cls.getSuperclass(), arrList);
    }

    // TODO : implement the interface scheme
    // private void buildInterfaceDependencyScheme ( Class<?> cls )
    // {
        
    // }

    // method that draws the entity
    public void draw ( Graphics gc )
    {


        int xB = App.W/2;
        int yB = App.H/2;
        int[] dimensions = computeDimension();
        int w = dimensions[0];
        int h = dimensions[1];

        // COLOR ALWAYS THE CLASS BOX WITH BLACK
        gc.setColor(BLACK);
        gc.fillRect(xB, yB, w, h);

        gc.setColor(WHITE);
        // we need to draw text
        int x = xB;
        int smallAdj = 1;
        int y = yB + offSet - smallAdj;
        String t = null;
    
        // iterate through all the fields
        for ( int i = 0 ; i < this.fields.length; i ++ )
        {

            t = fields[i];
            gc.drawString(t, x, y); 
            y += offSet;

        }

        // iterate through all the methods
        for ( int i = 0 ; i < this.methods.length; i ++ )
        {

            t = methods[i];
            gc.drawString(t, x, y); 
            y += offSet;

        }


    }

    public String repr ( ) { 
        String ret;
        ret = String.format ( "%s,\n [fields] : %s, \n [methods] : %s", cls, Arrays.toString (this.fields ) , Arrays.toString ( this.methods ) ); 
        return ret;
    }


    @Override
    public String toString ()
    {
        String ret = String.format("class : %s, use repr for more info ...", cls);
        return ret;
    }

}