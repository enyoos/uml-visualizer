package com.java.Editor;

import java.io.File;
import java.io.FileInputStream;
import java.awt.Dimension;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Parameter;
import java.awt.Font;
import java.awt.FontMetrics;

// import static com.java.Editor.Utilsation.Utils.*;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.util.EventListener;
import java.util.Optional;

import javax.swing.JFrame;

import com.java.Editor.Samples.A;

public class App 
{
    // this will be the size of the image
    public static final int W = 800;
    private static final String ttl = "debug";
    public static final int H = 800;
    // public static final Font font = new Font("Tahoma", 12);
    private static final String iconName = "logo.png";



    public static void main( String[] args )
    {

        Entity entity = new Entity(A.class);
        
        Dimension dim = new Dimension(W, H);
        JFrame win = new JFrame();

        MCanvas canvas = new MCanvas(entity);

        win.addWindowListener(new CustomListener());
        win.addKeyListener(new CustomListener());
        win.setResizable(false);
        win.setTitle(ttl);
        win.setSize(dim);
        win.add(canvas);
        win.setVisible(true);


    }
}

