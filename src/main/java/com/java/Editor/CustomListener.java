package com.java.Editor;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JFrame;

public class CustomListener implements KeyListener, WindowListener{

    @Override
    public void keyTyped(KeyEvent e) { }

    @Override
    public void keyPressed(KeyEvent e) {
        if ( e.getKeyCode() == KeyEvent.VK_ESCAPE ){
            JFrame jf =  ( JFrame ) e.getSource();
            jf.dispatchEvent(new WindowEvent(jf, WindowEvent.WINDOW_CLOSING));
        } 
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }

    @Override
    public void windowOpened(WindowEvent e) { }

    @Override
    public void windowClosing(WindowEvent e) {

        System.out.println("calling !");
        Utils.takeSS( ( JFrame ) e.getSource() , "output.png");
        (( JFrame ) e.getSource()).dispose();
        System.exit(0);
        
    }

    @Override
    public void windowClosed(WindowEvent e) { }

    @Override
    public void windowIconified(WindowEvent e) { }

    @Override
    public void windowDeiconified(WindowEvent e) { }

    @Override
    public void windowActivated(WindowEvent e) { }

    @Override
    public void windowDeactivated(WindowEvent e) { }
    
}
