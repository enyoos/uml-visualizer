package com.java.Editor;

import java.lang.reflect.Modifier;
import java.awt.FontMetrics;
import javax.swing.JFrame;
import java.awt.Graphics;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;

import javax.imageio.ImageIO;

import java.lang.reflect.Parameter;


public final class Utils {


    private final static String privateDenotatorUML = "-";
    private final static String protectedDenotatorUML = "#";
    private final static String publicDenotatorUML= "+";
    private final static String[] baseClassIgnoreJDK = new String[]{"Object", "String", "Integer", "BigInteger", "Double", "Float", "Byte", "Boolean", "Void", "Short", "Character", "Null"}; // is there a way to optimize that ?


    // the very naive solution : ^(AB|BC|MB|NB|NL|NS|NT|NU|ON|QC|PE|SK|YT){2} | use regex ?
    public final static boolean isIn ( String word )
    {
        for ( String baseCls : baseClassIgnoreJDK ) { if ( word.contains(baseCls) ) return true; }
        return false;
    }

    public static String outputStringAccordingToModifier ( int m )
    {
        if ( m == 1 ) return publicDenotatorUML;
        if ( m == 4 ) return protectedDenotatorUML;
        if ( m == 2 ) return publicDenotatorUML;
        else return "";
    }

    public static void takeSS ( JFrame jf, String fn ) {

        BufferedImage img = new BufferedImage(jf.getWidth(), jf.getHeight(), BufferedImage.TYPE_INT_RGB);
        jf.paint(img.getGraphics());
        File outputfile = new File(fn);
        try {
            ImageIO.write(img, "png", outputfile);
        }
        catch( IOException e ) 
        {
            e.printStackTrace();
        }
       
        System.out.println("wrote : "  + fn );
    }

    // building Methods
    public static String[] buildMethods ( Class <?> cls )
    {
        Method[] temp = cls.getDeclaredMethods();
        int len = temp.length;
        String[] ret = new String[len];
        for ( int i = 0; i < len ; i ++ )
        { // [name] ( [args ] : [type ]) : [return Type ]
            ret[i] = buildMethod(temp[i]);
        }
        return ret;
    }

    public static String buildMethod ( Method m )
    {
        String mName = m.getName();
        String mModifier = Utils.outputStringAccordingToModifier(m.getModifiers());
        String returnType = m.getReturnType().getSimpleName();
        String args  = buildArgs ( m.getParameters() );
        return String.format( "%s %s(%s) : %s", mModifier, mName, args, returnType);
    }


    // TODO : find a way to do this ... 
    public static int getWStr ( String c, Graphics context )
    {
        Rectangle2D r = context.getFontMetrics().getStringBounds(c, context);
        return ( int ) r.getWidth();
    }


    public static String buildArgs ( Parameter[] params )
    {
        int len = params.length;
        String ret = "";
        for ( int i = 0 ; i < len ; i ++ ) { ret += buildArg(params[i]) + ", "; }
        return ret;
    }

    
    public static String buildArg ( Parameter param )
    {
        // [name] : [type]
        String name = param.getName();
        int modifier = param.getModifiers();
        String argModifier = Utils.outputStringAccordingToModifier(modifier);
        String type = param.getType().getSimpleName();
        return String.format ( "%s %s : %s", argModifier, modifier == 16 ? name.toUpperCase() : name , type );
    }

    public static String buildField ( Field param )
    {
        // [name] : [type]
        String name = param.getName();
        int modifier = param.getModifiers();
        String argModifier = Utils.outputStringAccordingToModifier(modifier);
        String type = param.getType().getSimpleName();
        return String.format ( "%s %s : %s", argModifier, modifier == 16 ? name.toUpperCase() : name , type );
    }

    // building attributes 
    public static String[] buildFields ( Class<?> cls )
    {
        Field[] temp = cls.getFields();
        int len = temp.length;
        String[] ret = new String[len];
        for ( int i = 0; i < len ; i ++ )
        { // [name] ( [args ] : [type ]) : [return Type ]
            ret[i] = buildField(temp[i]);
        }
        return ret;
    }

    public static void quickSort(String arr[], int begin, int end) {
        // is this even necessary ? 
        if ( arr.length == 0 ) { return; }
        else if (begin < end) {
            int partitionIndex = partition(arr, begin, end);

            quickSort(arr, begin, partitionIndex-1);
            quickSort(arr, partitionIndex+1, end);
        }
    }

    private static int partition(String arr[], int begin, int end) {
        String pivot = arr[end];
        int i = (begin-1);
    
        for (int j = begin; j < end; j++) {
        if (arr[j].length() <= pivot.length()) {
                i++;
    
                String swapTemp = arr[i];
                arr[i] = arr[j];
                arr[j] = swapTemp;
        }
        }
    
        String swapTemp = arr[i+1];
        arr[i+1] = arr[end];
        arr[end] = swapTemp;
    
        return i+1;
    }


}