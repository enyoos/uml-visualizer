# Learning javafx

backend of javafx ; graphic engine called Prism, small efficient windowing system called Glass, a media engine and a web engine.

The scene graph is the starting point for constructing a javaFx app. -> tree of nodes.
Each node has an ID, style class, and volume.
First node : is the root node ( a leaf node doesn't have any children ).

Thread : app main thread, prism render thread and the media thread.
Pulse is an event that tells to fx scene grph to synchronize the state of the elements with prism. ( happens whenever something changes in the ui )
Using css at runtime!

# Exception and runtime ?


# cool things along the way
```java
    public static void drawArrowImplementation ( int x1, int y1, int x2, int y2, GraphicsContext context )
    {
        double dx = x2 - x1, dy = y2 - y1;
        double angle = Math.atan2(dy, dx);

        int len = (int) Math.sqrt(dx * dx + dy * dy);
        int offSetX = (int) dx/10;
        int offSetY = ( int ) dy/10;

        for ( int i = 0; i < 10 ; i++ )
        {
            drawLine(x1 += offSetX, y1 += offSetY, x2 += offSetX , y2 += offSetX, context, angle);
        }

        context.fillPolygon(new double[]{len, len - ARR_SIZE, len - ARR_SIZE, len}, new double[]{0, -ARR_SIZE, ARR_SIZE, 0}, 4);
    }
```

what I want : check if there's space for drawing objects
first check if we can draw the box including ( virtually ) the arrow